//
//  ToDoAppTests.swift
//  ToDoAppTests
//
//  Created by Leethu Mathai on 20/7/18.
//  Copyright © 2018 Leethu Mathai. All rights reserved.
//

import XCTest
@testable import ToDoApp
@testable import RealmSwift

class ToDoAppTests: XCTestCase {
    
    let config = Realm.Configuration(fileURL: nil, inMemoryIdentifier: "test", encryptionKey: nil, readOnly: false, schemaVersion: 0, migrationBlock: nil, objectTypes: nil)
    
    
    func testAddData() {
        let dataManager = DataManager(configuration: config)
        let todo = ToDo()
        todo.itemName = "1"
        todo.itemStatus = false
        dataManager.save(toDo: todo)
        let items = dataManager.retrieveToDoArray()
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "1")
        XCTAssertFalse(items[0].itemStatus)
    }
    
    func testUpdateData() {
        let dataManager = DataManager(configuration: config)
        let todo = ToDo()
        todo.itemName = "first Task"
        todo.itemStatus = false
        dataManager.save(toDo: todo)
        
        let savedTodo = dataManager.retrieveToDoArray().first
        dataManager.save(toDo: savedTodo!, with: true)
        let items = dataManager.retrieveToDoArray()
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName,"first Task")
        XCTAssertTrue(items[0].itemStatus)
    }
    
    func testDeleteData() {
        let dataManager = DataManager(configuration: config)
        let todo = ToDo()
        todo.itemName = "New Task"
        todo.itemStatus = false
        dataManager.save(toDo: todo)
        XCTAssertEqual(dataManager.retrieveToDoArray().count,1)
        dataManager.delete(toDo: todo)
        XCTAssertEqual(dataManager.retrieveToDoArray().count,0)
        
    }
}
