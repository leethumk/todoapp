//
//  Realm+Utils.swift
//  ToDoApp
//
//  Created by Leethu Mathai on 22/7/18.
//  Copyright © 2018 Leethu Mathai. All rights reserved.
//

import Foundation
import RealmSwift

extension Results{
    
    func toArray() -> [Element] {
        return self.map{$0}
    }
}
