//
//  ToDo.swift
//  ToDoApp
//
//  Created by Leethu Mathai on 21/7/18.
//  Copyright © 2018 Leethu Mathai. All rights reserved.
//

import Foundation
import RealmSwift

class ToDo: Object{
    @objc dynamic var itemId :String = UUID().uuidString
    @objc dynamic var itemName :String = ""
    @objc dynamic var itemStatus:Bool = false
    
    override static func primaryKey() -> String? {
        return "itemId"
    }
}
