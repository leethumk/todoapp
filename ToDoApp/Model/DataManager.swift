//
//  DataManager.swift
//  ToDoApp
//
//  Created by Leethu Mathai on 22/7/18.
//  Copyright © 2018 Leethu Mathai. All rights reserved.
//

import Foundation
import RealmSwift

class DataManager{
    
    let config: Realm.Configuration
    
    init(configuration: Realm.Configuration) {
        self.config = configuration
    }
    
    init() {
        guard let fileUrl = DataManager.fileUrl(for: "ToDo.realm") else {
            fatalError()
        }
        self.config = Realm.Configuration(
            fileURL: fileUrl,
            readOnly: false)
    }
    
    lazy var realm : Realm? = {
        let realm = try? Realm(configuration: config)
        return realm
    }()
    
    static func fileUrl(for fileName:String)-> URL?{
        guard let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        return documentsUrl.appendingPathComponent(fileName)
    }
    
    func save(toDo:ToDo, with status: Bool = false){
        guard let realm = realm else {
            fatalError()
        }
        do {
            try realm.write {
                toDo.itemStatus = status
                realm.add(toDo, update: true)
            }
        }catch{
            fatalError()
        }
    }
    
    func delete(toDo:ToDo) {
        guard let realm = realm else {
            fatalError()
        }
        do {
            try realm.write {
                realm.delete(toDo)
            }
        }catch{
            fatalError()
        }
    }
    
    func retrieveToDoArray()->[ToDo]
    {
        guard let realm = realm else {
            fatalError()
        }
        return realm.objects(ToDo.self).toArray().reversed() as [ToDo]
    }
    
}
