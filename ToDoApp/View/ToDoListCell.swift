//
//  ToDoListCell.swift
//  ToDoApp
//
//  Created by Leethu Mathai on 21/7/18.
//  Copyright © 2018 Leethu Mathai. All rights reserved.
//

import UIKit

class ToDoListCell: UITableViewCell {

    @IBOutlet weak var toDoItemLabel: UILabel!
    @IBOutlet weak var toDoStatusImage: UIImageView!
    
    func configureCellWith(data:ToDo)
    {
        toDoItemLabel.text = data.itemName
        setDoneStatus(status: data.itemStatus)
    }
    
    func setDoneStatus(status :Bool)
    {
        toDoStatusImage.isHighlighted = status
    }
}
