//
//  InputTableCell.swift
//  ToDoApp
//
//  Created by Leethu Mathai on 21/7/18.
//  Copyright © 2018 Leethu Mathai. All rights reserved.
//

import UIKit

protocol InputCellDelegate: class {
    func addButtonActionWith(data:String)
}

class InputTableCell: UITableViewCell {
    
    weak var delegate:InputCellDelegate?
    @IBOutlet weak var newItemTextField: UITextField!
    
    override func awakeFromNib() {
        newItemTextField.becomeFirstResponder()
        self.accessoryType = .none
        super.awakeFromNib()
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        
        if let text = newItemTextField.text, !text.isEmpty, !text.trimmingCharacters(in:CharacterSet.whitespacesAndNewlines).isEmpty
        {
            self.delegate?.addButtonActionWith(data: text)
        }
        newItemTextField.text = ""
    }

}
