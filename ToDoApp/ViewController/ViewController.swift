//
//  ViewController.swift
//  ToDoApp
//
//  Created by Leethu Mathai on 20/7/18.
//  Copyright © 2018 Leethu Mathai. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITableViewDataSource,UITableViewDelegate, InputCellDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    var toDoArray = [ToDo]()
    let dataManager = DataManager()
    
    private enum Constants {
        static let cellId = "TDTableCell"
        static let InputCellId = "InputCellId"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateData()
    }
    
    func updateUI()
    {
        tableView.reloadData()
    }
    
    func updateData()
    {
        toDoArray = dataManager.retrieveToDoArray()
    }
    
    // MARK:- TableView Data Source and Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var cell = UITableViewCell()
        if indexPath.row == 0
        {
            if let inputCell = tableView.dequeueReusableCell(withIdentifier: Constants.InputCellId, for: indexPath)as?InputTableCell
            {
                cell = inputCell
                inputCell.delegate = self
            }
        }
        else
        {
            let correctedIndex = indexPath.row - 1
            if let toDoCell = tableView.dequeueReusableCell(withIdentifier: Constants.cellId, for: indexPath) as? ToDoListCell,
                toDoArray.count > correctedIndex
            {
                    let toDoObject = toDoArray[correctedIndex]
                    toDoCell.configureCellWith(data: toDoObject)
                    cell = toDoCell
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) as? ToDoListCell,
            indexPath.row != 0
        {
            
            let correctedIndex = indexPath.row - 1
            var toDoObject:ToDo?
            if toDoArray.count > correctedIndex
            {
                toDoObject = toDoArray[correctedIndex]
                if let toDo = toDoObject
                {
                    let status = !toDo.itemStatus
                    dataManager.save(toDo: toDo, with: status)
                    cell.setDoneStatus(status: status)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if((tableView.cellForRow(at: indexPath as IndexPath) as? InputTableCell) != nil)
        {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { [weak self] ( action, IndexPath) in
            guard let this = self else { return }
            let correctedIndex = indexPath.row - 1
            if this.toDoArray.count > correctedIndex
            {
                this.dataManager.delete(toDo:this.toDoArray[correctedIndex])
                this.updateData()
                tableView.deleteRows(at:[indexPath], with: .fade)
            }
        }
        
        return [delete]
    }
    
    // MARK:- NewTableCell delegate method
    
    func addButtonActionWith(data toDoText:String)
    {
        let newToDo = ToDo()
        newToDo.itemName = toDoText
        dataManager.save(toDo: newToDo)
        updateData()
        let indexPath:IndexPath = IndexPath(row:1, section:0)
        tableView.insertRows(at:[indexPath] , with: .top)
    }
}
